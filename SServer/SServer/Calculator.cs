﻿using System.Text.RegularExpressions;
using System.Collections.Generic;
using System;
namespace SServer
{
   

    class Calculator
    {   
        delegate double MyDel(double x, double y);

        static Dictionary<char, MyDel> Operators = new Dictionary<char, MyDel>
            {
                {'+', (x,y)=> x + y},
                {'-', (x,y)=> x - y},
                {'*', (x,y)=> x * y},
                {'/', (x,y)=> x / y}
            };
        static Dictionary<char, int> Priority = new Dictionary<char, int>
        {
            {'*', 2},
            {'/', 2},
            {'+', 1},
            {'-', 1},
            {'(', 0},
            {')', 0}

        };

        private static bool CheckExpr(string s)
        {
            string match = "([0-9+-/*()])";
                        
            
            Regex reg = new Regex(match);
            if (reg.IsMatch(s))
                return true;
            else
                return false;
        }

        public static string FixBrackets(string s)
        {
            
            string res = "";
            for(int i = 0; i < s.Length; ++i)
            {
                if (s[i] == '{' || s[i] == '[')
                    res += '(';
                else if (s[i] == '}' || s[i] == ']')
                    res += ')';
                else
                    res += s[i];
            }
            return res;

        }
        private static bool CheckBrackets(string s)
        {
            Stack<char> Brackets = new Stack<char>();
            foreach(char c in s)
            {
                if (c == '(') Brackets.Push(c);
                else if(c == ')')
                {
                    if (Brackets.Count > 0)
                        Brackets.Pop();
                    else
                        return false;
                        
                }

            }
            if (Brackets.Count > 0)
                return false;
            else
                return true;

        }

        private static string GetRevPNotation(string s)
        {
            string res = "";
            Stack<char> Operators = new Stack<char>();
            bool needspace = false;
            foreach(char c in s)
            {
                if(char.IsDigit(c))
                {
                    needspace = true;
                    res += c;
                }
                else if(Priority.ContainsKey(c))
                {
                    if (needspace) { res += ' '; needspace = false; }
                    if (c == '(') Operators.Push(c);
                    else if(c == ')')
                    {
                        while(Operators.Peek() != '(')
                        {
                            res += Operators.Pop().ToString() + ' ';
                            
                        }
                        Operators.Pop();
                    }
                    else
                    {
                        while(Operators.Count > 0 && Priority[c] <= Priority[Operators.Peek()])
                        {
                            res += Operators.Pop().ToString() + ' ';
                        }
                        Operators.Push(c);
                    }
                }
            }
            if (needspace) res += ' ';
            needspace = false;
            while(Operators.Count > 0)
            {
                res += Operators.Pop().ToString() + ' ';
            }
            return res;
        }

        public static string Calculate(string s)
        {
            string res = FixBrackets(s);
            
            if(CheckExpr(res) && CheckBrackets(res))
            {
                res = GetRevPNotation(res);
                Stack<double> Nums = new Stack<double>();
                string[] substr = res.Split(' ');
                foreach(string str in substr)
                {
                    if (str == "") continue;
                    if(Operators.ContainsKey(str.ToCharArray()[0]))
                    {
                        if(Nums.Count >= 2)
                        {
                            double a = Nums.Pop();
                            double b = Nums.Pop();
                            
                            Nums.Push(Operators[str.ToCharArray()[0]](b, a));
                            if (a == 0 && str.ToCharArray()[0] == '/') return "Division by zero";
                            
                            
                        }
                        else
                        {
                            return "Incorrect Expression";
                        }
                    }
                    else
                    {
                        Nums.Push(double.Parse(str));
                    }
                }
                if (Nums.Count == 1)
                    return Nums.Pop().ToString();
                else
                    return "Incorrect Expression";
            }
            else
            {
                return "Incorrect Expression";
            }
        }
        

    }
}
