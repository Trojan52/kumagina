﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace SServer
{
    class Program
    {
        static void Main(string[] args)
        {
            IPAddress ipAddr = null;
            IPHostEntry ipHost = Dns.GetHostEntry(IPAddress.Parse("127.0.0.1"));
            foreach(IPAddress ip in ipHost.AddressList)
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddr = ip;
                    break;
                }
            }
            if (ipAddr == null) { Console.WriteLine("Couldn't init server..."); return; }
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

            
            Socket sListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);

                
                while (true)
                {
                   // Console.WriteLine("Ожидаем соединение через порт {0}", ipEndPoint);

                    
                    Socket handler = sListener.Accept();
                    string data = null;

                    

                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);

                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    if (data.IndexOf("stop") > -1)
                    {
                        Console.WriteLine("Сервер завершил соединение с клиентом.");
                        break;
                    }
                   
                    Console.Write("Полученный текст: " + data + "\n\n");
                    string reply = Calculator.Calculate(data);
                    Console.Write("Вычисленное значение = " + reply + "\n\n");
                    
                    
                    byte[] msg = Encoding.UTF8.GetBytes(reply);
                    handler.Send(msg);

                   

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }

        
    }
}
