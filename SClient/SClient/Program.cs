﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace SClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("IP > ");
                string sIP = Console.ReadLine();
                if (sIP == "") SendMessageFromSocket(11000);
                IPAddress IpA = IPAddress.Parse(sIP);
                SendMessageFromSocket(11000, IpA);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }


        static void SendMessageFromSocket(int port, IPAddress ip = null)
        {
            
            byte[] bytes = new byte[1024];
            IPAddress ipAddr = null;
            if (ip == null)
            {
                IPHostEntry ipHost = Dns.GetHostEntry(IPAddress.Parse("127.0.0.1"));
                foreach (IPAddress ipA in ipHost.AddressList)
                {
                    if (ipA.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddr = ipA;
                        break;
                    }
                }
                
                
            }
            else
            {
                ipAddr = ip;
            }
            if (ipAddr == null) { Console.WriteLine("Couldn't init client..."); Environment.Exit(0); }
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);

            Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            
            sender.Connect(ipEndPoint);

            Console.Write("CLIENT > ");
            string message = Console.ReadLine();

            //Console.WriteLine("Сокет соединяется с {0} ", sender.RemoteEndPoint.ToString());
            byte[] msg = Encoding.UTF8.GetBytes(message);

            
            int bytesSent = sender.Send(msg);

            
            int bytesRec = sender.Receive(bytes);

            Console.WriteLine("\nSERVER > {0}\n\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));

            
            if (message.IndexOf("stop") == -1)
                SendMessageFromSocket(port, ip);

            
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }
}
